<?php 

define('NAMA', 'Reza Satria'); //dapat didefinisikan dengan define
echo NAMA;

echo "<br>";

const UMUR = 29; //bisa juga menggunakan const
echo UMUR;

//define() tidak bisa digunakan di dalam sebuah class

class Coba {
    const NAMA_CLASS = 'Reza dalam Class';
}
echo "<br>";
echo Coba::NAMA_CLASS;

//Magic Constant
// __LINE__     Menampilkan baris dimana konstan ini ditulis
//__FILE__      Menampilkan path dan nama file yg bersangkutan
//__DIR__       Menampilkan directory
//__FUNCTION__  Menampillan nama fungsi dimana konstan ini ditulis
//__CLASS__  Menampillan nama class dimana konstan ini ditulis