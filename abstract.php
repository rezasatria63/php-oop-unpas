<?php 

abstract class Buah {
    private $warna;
    
    abstract public function makan(); //hanya interface. implementasinya ada di kelas turunannya

    public function setWarna($warna)
    {
        $this->warna = $warna;
    }
}

class Apel extends Buah {

    public function makan()
    {
        //kunyah
        //sampai tengahnya
    }
}

class Jeruk extends Buah {

    public function makan()
    {
        //kupas duluu
        //kunyah bang
    }
}