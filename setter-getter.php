<?php

class Produk {
    private $judul, //hanya bisa diakses oleh class produk
            $penulis,
            $penerbit,
            $harga,
            $diskon = 0;
    
    //protected $harga; //hanya bisa diakses oleh class produk dan turunannya

    public function __construct($judul = "Judul", $penulis = "Penulis", $penerbit = "Penerbit", $harga = 0)
    {
        $this->judul = $judul;
        $this->penulis = $penulis;
        $this->penerbit = $penerbit;
        $this->harga = $harga;
    }

    public function setJudul( $judul )
    {
        $this->judul = $judul;
    }
    public function getJudul()
    {
        return $this->judul;
    }

    public function setPenulis( $penulis )
    {
        $this->penulis = $penulis;
    }
    public function getPenulis()
    {
        return $this->penulis;
    }

    public function setPenerbit( $penerbit )
    {
        $this->penerbit = $penerbit;
    }
    public function getpenerbit()
    {
        return $this->penerbit;
    }

    public function setHarga( $harga )
    {
        $this->harga = $harga;
    }

    public function getHarga()
    {
        return $this->harga - ($this->harga * $this->diskon / 100);
    }
        
    public function setDiskon($diskon)
    {
        $this->diskon = $diskon;
    }

    public function getDiskon()
    {
        return $this->diskon;
    }

    public function getLabel()
    {
        return "{$this->penulis}, $this->penerbit"; //pakai {} supaya lebih rapih

    }

    public function getInfoProduk()
    {
        $str = "{$this->judul} | {$this->getLabel()} (Rp {$this->harga})";
        
        return $str;
    }

}

class Komik extends Produk {
    public $jmlHalaman;

    public function __construct($judul = "Judul", $penulis = "Penulis", $penerbit = "Penerbit", $harga = 0, $jmlHalaman=0)
    {
        parent::__construct($judul, $penulis, $penerbit, $harga);
        $this->jmlHalaman = $jmlHalaman;

    }

    public function getInfoProduk()
    {
        $str = "Komik: " . parent::getInfoProduk() . " | {$this->jmlHalaman} Halaman"; //parent:: untuk override method getInfoProduk() dari parent class
        
        return $str;
    }
}

class Game extends Produk {
    public $waktuMain;

    public function __construct($judul = "Judul", $penulis = "Penulis", $penerbit = "Penerbit", $harga = 0, $waktuMain=0)
    {
        parent::__construct($judul, $penulis, $penerbit, $harga);

        $this->waktuMain = $waktuMain;

    }

    public function getInfoProduk()
    {
        $str = "Game: " . parent::getInfoProduk() . " | {$this->waktuMain} Jam";
        
        return $str;
    }
}

//object type
class CetakInfoProduk {
    public function cetak(Produk $produk) //menggunakan object type sebagai parameter
    {
        $str = "{$produk->judul} | {$produk->getLabel()} (Rp {$produk->harga}";
        return $str;
    }
}
//object type end

$produk1 = new Komik("Naruto", "Masashi K", "Shonen", 30_000, 200);
$produk2 = new Game("GTA", "CJ", "Rockstar Game", 200_000,40);

echo $produk1->getInfoProduk();
echo "<br>";
echo $produk2->getInfoProduk();
echo "<hr>";

//$produk2->harga = 3000;
//echo $produk1->judul;
$produk2->setDiskon(50);
echo $produk2->getHarga();

echo '<hr>';
//$produk1->judul = "Judul Baru"; //ini tidak bisa dilakukan karena $judul visibility-nya private
$produk1->setJudul("Judul Baru"); //ini baru bisa
$produk1->setDiskon(45);
echo $produk1->getDiskon();
echo "<br>";
echo $produk1->getJudul();
echo "<br>";
echo $produk1->getHarga();

