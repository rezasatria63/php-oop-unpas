<?php 

class Produk {
    public $judul,
            $penulis,
            $penerbit;

    protected $diskon = 0;
    //protected $harga; //hanya bisa diakses oleh class produk dan turunannya
    private $harga; //hanya bisa diakses oleh class produk

    public function __construct($judul = "Judul", $penulis = "Penulis", $penerbit = "Penerbit", $harga = 0)
    {
        $this->judul = $judul;
        $this->penulis = $penulis;
        $this->penerbit = $penerbit;
        $this->harga = $harga;
    }

    public function getHarga()
    {
        return $this->harga - ($this->harga * $this->diskon / 100);
    }

    public function getLabel()
    {
        return "{$this->penulis}, $this->penerbit"; //pakai {} supaya lebih rapih

    }

    public function getInfoProduk()
    {
        $str = "{$this->judul} | {$this->getLabel()} (Rp {$this->harga})";
        
        return $str;
    }

}

class Komik extends Produk {
    public $jmlHalaman;

    public function __construct($judul = "Judul", $penulis = "Penulis", $penerbit = "Penerbit", $harga = 0, $jmlHalaman=0)
    {
        parent::__construct($judul, $penulis, $penerbit, $harga);
        $this->jmlHalaman = $jmlHalaman;

    }

    public function getInfoProduk()
    {
        $str = "Komik: " . parent::getInfoProduk() . " | {$this->jmlHalaman} Halaman"; //parent:: untuk override method getInfoProduk() dari parent class
        
        return $str;
    }
}

class Game extends Produk {
    public $waktuMain;

    public function __construct($judul = "Judul", $penulis = "Penulis", $penerbit = "Penerbit", $harga = 0, $waktuMain=0)
    {
        parent::__construct($judul, $penulis, $penerbit, $harga);

        $this->waktuMain = $waktuMain;

    }
    
    public function setDiskon($diskon)
    {
        $this->diskon = $diskon;
    }

    public function getInfoProduk()
    {
        $str = "Game: " . parent::getInfoProduk() . " | {$this->waktuMain} Jam";
        
        return $str;
    }
}

//object type
class CetakInfoProduk {
    public function cetak(Produk $produk) //menggunakan object type sebagai parameter
    {
        $str = "{$produk->judul} | {$produk->getLabel()} (Rp {$produk->harga}";
        return $str;
    }
}
//object type end

$produk1 = new Komik("Naruto", "Masashi K", "Shonen", 30_000, 200);
$produk2 = new Game("GTA", "CJ", "Rockstar Game", 200_000,40);

echo $produk1->getInfoProduk();
echo "<br>";
echo $produk2->getInfoProduk();
echo "<hr>";

//$produk2->harga = 3000;
$produk2->setDiskon(50);
echo $produk2->getHarga();