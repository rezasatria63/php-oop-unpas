<?php 

class ContohStatic{
    public static $angka = 1;

    public static function halo()
    {
        return "Halo... " . self::$angka++ . " kali."; //pada static keyword kita menggunakan self utk merujuk ke property di dalam class
    }

}

echo ContohStatic::$angka;
echo "<br>";
echo ContohStatic::halo();
echo "<br>";
echo ContohStatic::halo();
echo "<hr>";

//////////////

class Contoh{
    public static $angka = 1;

    public  function halo()
    {
        return "Halo... " . self::$angka++ . " kali."; //pada static keyword kita menggunakan self utk merujuk ke property di dalam class
    }

}

$obj = new Contoh;
echo $obj->halo();
echo "<br>";
echo $obj->halo();
echo "<br>";
echo $obj->halo();
echo "<br>";
echo $obj->halo();
echo "<br>";

echo "<hr>";

//walaupun instansiasi object baru, nilai $angka tidak ter-reset
$obj2 = new Contoh;
echo $obj2->halo();
echo "<br>";
echo $obj2->halo();
echo "<br>";
echo $obj2->halo();
echo "<br>";
echo $obj2->halo();
echo "<br>";

